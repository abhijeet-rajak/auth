const { Model, DataTypes, INTEGER } = require("sequelize");
const sequelize = require("./index");
class AcessToken extends Model {}

const accessTokenSchema = {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  access_token: {
    type: DataTypes.STRING,
  },
  login_time: {
    type: DataTypes.DATE,
  },
  logout_time: {
    type: DataTypes.DATE,
  },
  user_id: {
    type: DataTypes.INTEGER,
  },
};

// User.hasMany(Otp, {
//   foreignKey: "user_id",
// });

AcessToken.init(accessTokenSchema, {
  sequelize,
  tableName: "accesstoken",
});

// AcessToken.sync({}).then(console.log("Access token synced!"));

module.exports = AcessToken;
