const { Model, DataTypes } = require("sequelize");
const sequelize = require("../models/index");
const Otp = require("../models/Otp");
const AcessToken = require("./AccessToken");
const AcesToken = require("./AccessToken");

class User extends Model {}

const userSchema = {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  first_name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  last_name: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING,
    isEmail: true,
    allowNull: false,
    unique: true,
  },
  phone: {
    type: DataTypes.STRING,
  },
  is_active: {
    type: DataTypes.BOOLEAN,
  },
};

// User.hasMany(Otp, {
//   foreignKey: "user_id",
// });

User.init(userSchema, {
  sequelize,
  tableName: "user",
});
User.hasOne(AcesToken, {
  foreignKey: "user_id",
});
User.hasOne(Otp, {
  foreignKey: "user_id",
});
Otp.belongsTo(User, {
  foreignKey: "user_id",
});
AcessToken.belongsTo(User, {
  foreignKey: "user_id",
});

// User.sync({}).then(console.log("User is synced !"));

module.exports = User;
