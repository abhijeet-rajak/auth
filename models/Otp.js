const { Model, DataTypes } = require("sequelize");
const sequelize = require("../models/index");

class Otp extends Model {}

const otpSchema = {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  is_revoked: {
    type: DataTypes.BOOLEAN,
    defaultValue: false,
  },
  otp: {
    type: DataTypes.INTEGER(6),
  },
  user_id: {
    type: DataTypes.INTEGER,
  },
  expiry_time: {
    type: DataTypes.DATE,
  },
  type: {
    type: DataTypes.STRING,
    defaultValue: "email",
  },
};

Otp.init(otpSchema, {
  sequelize,
  tableName: "otp",
});

// Otp.sync({}).then(console.log("Opt is synced !"));

module.exports = Otp;
