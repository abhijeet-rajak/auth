const { verify } = require("jsonwebtoken");
const Config = require("../config/config.json");

const auth = {
  name: "auth-middleware",
  register: (server, option, next) => {
    return {
      authenticate: function (request, h) {
        console.log(request.headers.authorization);
        try {
          const verifiedToken = verify(
            request.headers.authorization,
            process.env.PRIVATE_KEY
          );
          console.log(verifiedToken);
          request.id = verifiedToken.id;
          return h.continue;
        } catch (err) {
          console.log(err);
          // console.log(new AppError(err.message, err.name, 403));
          // return new AppError(err.message, err.name, 403);
          return h.response(err.message).code(403).takeover();
        }
      },
    };
  },
};

module.exports = auth;
