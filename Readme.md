# Welcome To PasswordLessAuth System Api:

This is backend applicationbuild using nodejs and hapi.js, database used is MYSQL

# Clone and versioning:

Clone the repo using command : git clone https://Abhijeet_Rajak@bitbucket.org/abhijeet-rajak/auth.git

# Setup Process:

1. install node version v12.13.1
2. npm i to install all the dependencies
3. install nodemon as dev dependencies
4. run command npx sequelize-cli init for project bootstrapping
5. run the server using nodemon server

once the server is running go to localhost://8000/documentation
which is api documentation created by using Swagger
