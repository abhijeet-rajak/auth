const nodemailer = require("nodemailer");
const sgTransport = require("nodemailer-sendgrid-transport");
const otpGenerator = require("otp-generator");
const { sign } = require("jsonwebtoken");
const User = require("../models/User");
const Otp = require("../models/Otp");
const AcessToken = require("../models/AccessToken");

const options = {
  auth: {
    api_key: process.env.SEND_GRID_API_KEY,
  },
};

const mailer = nodemailer.createTransport(sgTransport(options));

const updateOrCreate = async (id, otp) => {
  const user = await Otp.findOne({
    where: {
      user_id: id,
    },
  });

  let addMin = new Date();
  let expiry_time = new Date(addMin.getTime() + 2 * 1000 * 60);

  if (!user) {
    await Otp.create({
      user_id: id,
      expiry_time,
      otp,
    });
  } else {
    await Otp.update(
      {
        otp,
        expiry_time,
        is_revoked: false,
      },
      {
        where: {
          user_id: id,
        },
      }
    );
  }
};

// const updateOrCreate1 = async(Model);

module.exports = {
  async send(request, h) {
    try {
      const { email } = request.payload;
      let user;
      user = await User.findOne({
        where: {
          email,
        },
      });

      if (!user) {
        throw new Error("User not authorized !");
      }

      let otp = otpGenerator.generate(6, {
        upperCase: false,
        specialChars: false,
        digits: true,
        alphabets: false,
      });

      // store the otp

      updateOrCreate(user.getDataValue("id"), otp);
      //   Otp.createOrUpdate(email, user.getDataValue("id"));

      let sendEmail = {
        to: email,
        from: "rajakabhijeet6@gmail.com",
        subject: "OTP",
        // text: "Awesome sauce",
        html: `<b>Your Otp :${otp}</b>`,
      };

      mailer.sendMail(sendEmail, function (err, res) {
        if (err) {
          console.log(err);
        }
        console.log(res);
      });

      return h.response("otp send please check your mail !").code(200);
    } catch (err) {
      return h.response({ message: err.message }).code(400);
    }
  },
  async submit(request, h) {
    try {
      const { email, sendOtp } = request.payload;
      let is_revoked, expiry_time, otp;

      // check otp is revoked or not
      const res = await User.findOne({
        include: [
          {
            model: Otp,
            attributes: ["is_revoked", "expiry_time", "otp"],
          },
        ],
        where: {
          email,
        },
        attributes: ["id"],
      });
      if (!res) throw new Error("Please try to send Otp again !");

      is_revoked = res.getDataValue("Otp").getDataValue("is_revoked");
      expiry_time = res.getDataValue("Otp").getDataValue("expiry_time");
      otp = res.getDataValue("Otp").getDataValue("otp");

      // match the content of the otp
      if (otp !== sendOtp) throw new Error("otp didnot matched try again !");
      // check whether revoked or not
      if (is_revoked) throw new Error("cannot use already revoked otp");

      // check for expiry

      if (new Date(expiry_time).toISOString() < new Date().toISOString()) {
        throw new Error("otp expired please try again !");
      }

      await Otp.update(
        {
          is_revoked: true,
        },
        {
          where: {
            user_id: res.getDataValue("id"),
          },
        }
      );
      // generate jwt Token
      let id = res.getDataValue("id");
      const token = sign({ id: id }, process.env.PRIVATE_KEY, {
        expiresIn: 60 * 10,
      });

      const acsessToken = await AcessToken.findOne({
        where: {
          user_id: id,
        },
      });
      if (!acsessToken) {
        await AcessToken.create({
          user_id: id,
          login_time: new Date().toISOString(),
          access_token: token,
        });
      } else {
        await AcessToken.update(
          {
            login_time: new Date().toISOString(),
            access_token: token,
          },
          {
            where: {
              user_id: id,
            },
          }
        );
      }

      return h
        .response({ msg: "Welcome to login page", access_token: token })
        .code(200);
    } catch (err) {
      // console.log(err);
      return h.response(err.message).code(400);
    }
  },
  async logout(request, h) {
    try {
      const currentUser = request.id;
      console.log("currentUser", currentUser);
      const user = await AcessToken.update(
        {
          access_token: null,
          logout_time: new Date().toISOString(),
        },
        {
          where: {
            user_id: currentUser,
          },
        }
      );
      console.log(user);
      if (user[0]) return h.response("Thank you visit agian !").code(200);
      return h.response("Please login !");
    } catch (err) {
      h.response(err.message).code(400);
    }
  },
  async test(request, h) {
    try {
      // const user = await User.bulkCreate([
      //   {
      //     first_name: "Abhijeet",
      //     last_name: "Rajak",
      //     email: "abhijeetrajak10@gmail.com",
      //     phone: "8008008001",
      //     is_active: true,
      //   },
      //   {
      //     first_name: "Avirav",
      //     last_name: "Rajak",
      //     email: "avirav01@gmail.com",
      //     phone: "8008008001",
      //     is_active: true,
      //   },
      //   {
      //     first_name: "Siddhi",
      //     last_name: "Raj",
      //     email: "rajakabhijeet6@gmail.com",
      //     phone: "8008008001",
      //     is_active: true,
      //   },
      // ]);

      const a = await AcessToken.findOne({
        id: 1,
      });
      return h.response("this is test route").code(200);
    } catch (err) {
      console.log(err);
    }
  },
};
