const Hapi = require("@hapi/hapi");
const Inert = require("@hapi/inert");
const Vision = require("@hapi/vision");
const HapiSwagger = require("hapi-swagger");
const auth = require("./middleware/auth");
const dotenv = require("dotenv");
dotenv.config();

const sequelize = require("./models/index");
sequelize
  .sync({ force: false })
  .then((res) => console.log("sucessfully synced"));

require("./models/sync");

const server = Hapi.server({
  host: "localhost",
  port: 8000,
});

async function start() {
  try {
    const swaggerOptions = {
      info: {
        title: "Test API Documentation",
        version: "1.0.0.0",
      },
    };

    await server.register([
      Inert,
      Vision,
      {
        plugin: HapiSwagger,
        options: swaggerOptions,
      },
    ]);

    server.auth.scheme("custom", auth.register);
    server.auth.strategy("default", "custom");
    server.auth.default("default");

    await server.register({
      plugin: require("hapi-dev-errors"),
      options: {
        showErrors: process.env.NODE_ENV !== "production",
        // useYouch: true,
      },
    });

    await server.register([require("./Routes/authRoutes")]);

    await server.start();
  } catch (err) {
    console.log(err);
    process.exit(1);
  }

  console.log("Server running at:", server.info.uri);
}

start();
