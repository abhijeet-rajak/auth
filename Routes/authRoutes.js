const Joi = require("joi");

const { send, submit, test, logout } = require("../Controller/authController");

// ("auth key: SG.sy7IEmjcRReIf5VYBdJRUw.R5RMU1ycOBX1XmfGdJcMPc1vQ0ZRDia0V27idXdUScg");

const authRoutes = {
  name: "auth",

  register: function (server, option) {
    const routes = [
      {
        method: "POST",
        path: "/send",
        config: {
          auth: false,
          description: "This is login route",
          notes: "payload email is passed",
          tags: ["api"],
          validate: {
            payload: Joi.object({
              email: Joi.string().required().email(),
            }),
          },
          handler: send,
        },
      },

      {
        method: "POST",
        path: "/submit",
        config: {
          auth: false,
          description: "This is login route",
          notes: "payload email is passed",
          tags: ["api"],
          validate: {
            payload: Joi.object({
              email: Joi.string().required().email(),
              sendOtp: Joi.number().required(),
            }),
          },
          handler: submit,
        },
      },

      {
        method: "DELETE",
        path: "/logout",
        config: {
          description: "This is logout route",
          notes: "no aguments are passed",
          tags: ["api"],
          handler: logout,
          validate: {
            headers: Joi.object({
              Authorization: Joi.string(),
            }).options({
              allowUnknown: true,
            }),
          },
        },
      },
      {
        method: "GET",
        path: "/test",
        config: {
          auth: false,
          description: "This test route",
          notes: "no aguments are passed",
          tags: ["api"],
          handler: test,
        },
      },
    ];
    server.route(routes);
  },
};

authRoutes.register.attributes = {
  name: "auth-routes",
  vesion: "1.0.0",
};

module.exports = authRoutes;
